const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const axios = require("axios")

app.use(bodyParser.json())

mongoose.connect("mongodb+srv://venu:7swARAs59@ordersservice-wdkf0.mongodb.net/test?retryWrites=true&w=majority", 
{useUnifiedTopology: true, useNewUrlParser: true},
() => {
    console.log("Database connected - Orders Service")
})

// Load the order model
require("./Order")
const Order = mongoose.model("Order")

// Create new order
app.post("/order", (req, res) => {
     var newOrder = {
         CustomerID: mongoose.Types.ObjectId(req.body.CustomerID),
         BookID: mongoose.Types.ObjectId(req.body.BookID),
         initialDate: req.body.initialDate,
         deliveryDate: req.body.deliveryDate
     }

     var order = new Order(newOrder)

     order.save().then(() => {
        console.log("Order created with success!")
        res.send("Order created with success!") 
     }).catch((err) => {
         if(err){
             throw err;
         }
     })
})

app.get("/orders", (req,res) => {
    Order.find().then((books) => {
        res.json(books)
    }).catch((err) => {
        if(err) {
            throw err;
        }
    })
})

app.get("/order/:id", (req, res) => {

    Order.findById(req.params.id).then((order) => {

        if (order){
            // Do process order by making request to customer and book services
            axios.get("http://localhost:5555/customer/" + order.CustomerID).then((response) => {
                console.log(response)
                var orderObject = {
                    customerName: response.data.name,
                    bookTitle: ''
                }
                axios.get("http://localhost:4545/book/" + order.BookID).then((response) => {
                    orderObject.bookTitle = response.data.title
                    res.json(orderObject)
                })
            })
    
        } else {
            res.send("Invalid order.")
        }
    })
    
})

// Beware of some unsafe ports, google them up
app.listen(6676, () => {
    console.log("Up and running - Orders Service")
})