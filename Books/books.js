// Microservice for Books

// Load express
const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());

// Load mongoose
const mongoose = require("mongoose");

require("./book")
const Book = mongoose.model("Book")

// Connect to DB
mongoose.connect("mongodb+srv://venu:7swARAs59@booksservice-emi4q.mongodb.net/test?retryWrites=true&w=majority",
{useUnifiedTopology: true, useNewUrlParser: true},  () => {
    console.log("Database is connected!");
})

// Create first route in the application
// A simple express route
app.get('/', (req,res) => {
    res.send("This is our main endpoint! This is our Books service.");
})

// Create a book
app.post("/book", (req, res) => {
    //console.log(req.body);
    //res.send("Testing our book route! WOW!")
    var newBook = {
        title: req.body.title,
        author: req.body.author,
        numberPages: req.body.numberPages,
        publisher: req.body.publisher
    }
    // create a new book 
    var book = new Book(newBook)

    book.save().then(() => {
        console.log("New book created!")
    }).catch((err) => {
        if (err) {
            throw err;
        }
    })
    // This is required to close the DB Transaction
    res.send("A new book created successfully in database!")
})

// Get a list of all books in DB
app.get("/books", (req,res) => {
    Book.find().then ((books) => {
        console.log(books)
        res.json(books)
    }).catch((err) => {
        if(err) {
            throw err;
        }
    })
})

// Get a book of interest
app.get("/book/:id", (req, res) => {
    //res.send(req.params.id)
    Book.findById(req.params.id).then((book) => {
         if(book){
             // show book data in browser
             res.json(book)
         } else {
             res.sendStatus(404);
         }
    }).catch(err => {
        if(err){
            throw err;
        }
    })
})

// Delete a book from DB
app.delete("/book/:id", (req, res) => {
    Book.findByIdAndRemove(req.params.id).then(() => {
        res.send("Book removed with success!")
    }).catch((err) => {
        if(err) {
            throw err;
        }
    })
})

app.listen(4545, () => {
    console.log("Up and running ! -- This is our Books service");
})

