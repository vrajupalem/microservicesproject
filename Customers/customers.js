const express = require("express")
const app = express()
const mongoose = require("mongoose")
const bodyParser = require("body-parser");

app.use(bodyParser.json());

// Connect to DB
mongoose.connect("mongodb+srv://venu:7swARAs59@customerservices-cl5lg.mongodb.net/test?retryWrites=true&w=majority", 
{useUnifiedTopology: true, useNewUrlParser: true},
() => {
    console.log("Database connected - Customers Service")
})

// Get the model for customer
require("./Customer")
const Customer = mongoose.model("Customer")

// Create new customer
app.post("/customer", (req,res) => {
    var newCustomer = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address
    }

    var customer = new Customer(newCustomer)

    customer.save().then(() => {
        res.send("New Customer created")
    }).catch ((err) => {
        if(err) {
            throw err;
        }
    })
})

// Get a list of all customers in DB
app.get("/customers", (req,res) => {
    Customer.find().then ((customers) => {
        console.log(customers)
        res.json(customers)
    }).catch((err) => {
        if(err) {
            throw err;
        }
    })
})

// Get a customer of interest
app.get("/customer/:id", (req, res) => {
    //res.send(req.params.id)
    Customer.findById(req.params.id).then((customer) => {
         if(customer){
             // show customer data in browser
             res.json(customer)
         } else {
             res.sendStatus(404);
         }
    }).catch(err => {
        if(err){
            throw err;
        }
    })
})

// Delete a customer from DB
app.delete("/customer/:id", (req, res) => {
    Customer.findByIdAndRemove(req.params.id).then(() => {
        res.send("Customer removed with success!")
    }).catch((err) => {
        if(err) {
            throw err;
        }
    })
})


app.listen("5555", () => {
    console.log("Up and runnng - Customers service ...")
})